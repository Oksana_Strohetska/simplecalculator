package user.generator;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class NumberGenerator {
    Random random = new Random();

    public List<Integer> generateNumbers(int count) {
        return IntStream
                .rangeClosed(0, count)
                .mapToObj(i -> random.nextInt(1000000))
                .collect(toList());
    }
}
