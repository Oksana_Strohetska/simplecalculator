package main.java.operations.imp;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DivideOperationTest {

    private DivideOperation divideOperation;

    @BeforeEach
    public void setUp() {
        divideOperation = new DivideOperation();
    }

    @Test
    public void name() {
        double actual = divideOperation.apply(10.0, 2.0);
        double expected = 5.0;

        assertEquals(expected, actual, 0.1);
    }

    @AfterEach
    public void tearDown() {
        divideOperation = null;
    }
}