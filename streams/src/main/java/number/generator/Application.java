package number.generator;

import number.generator.exceptions.AssertionException;
import org.flywaydb.core.Flyway;
import user.db.connection.DBConfig;
import user.db.connection.DataBaseConnectionProvider;
import user.generator.NumberGenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {

       public static void main(String[] args) {
        NumberGenerator numberGenerator = new NumberGenerator();
        DataBaseConnectionProvider provider = new DataBaseConnectionProvider(new DBConfig());
        NumberStorage numberStorage = new NumberStorageImpl(provider);

        dBMigrate();

        //case#1
        numberStorage.deleteAll();
        Integer insertNumber = 100500;
        numberStorage.insert(insertNumber);
        Integer actual = numberStorage.get(1);
        checkNumber(actual,insertNumber);

        //case#3
        numberStorage.deleteAll();
        List<Integer> numbers = numberGenerator.generateNumbers(100);
        numberStorage.insert(getParamsFromNumbersList (numbers));
        List<Integer> actualList = numberStorage.getAll();
        checkAllNumbers(actualList, numbers);

        //case#4
        numberStorage.get(1);
        numberStorage.delete(1);
        actual = numberStorage.get(1);
        checkNumber(actual, 0);

        numberStorage.close();
    }

    private static void checkAllNumbers(List<Integer> actualList, List<Integer> expectedList) {
        String message = String.format("Actual list [] and expected list [] of numbers are not matched", actualList, expectedList);
        if (!actualList.equals(expectedList)) {
            throw new AssertionException(message);
        }
    }
    private static void checkNumber(Integer actual, Integer expected) {
        String message = String.format("Actual and expected numbers are not the same", actual, expected);
        if (!actual.equals(expected)) {
            throw new AssertionException(message);
        }
    }

    private static Map <Integer, Object> getParamsFromNumbersList(List<Integer> numbers) {
           Map <Integer, Object> params = new HashMap<>();
           for (int i = 1; i <= numbers.size(); i++) {
               params.put(i, numbers.get(i-1));
           }
           return params;
    }

    private static void dBMigrate() {
        DBConfig config = new DBConfig();
        Flyway flyway = Flyway
                .configure()
                .dataSource("jdbc:postgresql://" + config.getDbHost() +":" + config.getDbPort() + "/" +
                        config.getDbName(), config.getDbUser(), config.getDbPass())
                .load();
        flyway.baseline();
        flyway.migrate();
    }
}
