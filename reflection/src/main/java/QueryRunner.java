import db.connection.DBConnectionProvider;
import entityPropsGenerator.EntityProps;

import java.sql.*;
import java.util.List;

public class QueryRunner {

    private final Connection connection;

    public QueryRunner(DBConnectionProvider connectionProvider) {
        connection = connectionProvider.getDBConnection();
    }

    public int runInsertQuery(List<EntityProps> entityProps, String query) throws SQLException{
        PreparedStatement ps = connection.prepareStatement(query);
        setDataToPreparedStatement(entityProps, ps);
        return ps.executeUpdate();
    }

    private PreparedStatement setDataToPreparedStatement(List<EntityProps> entityProps, PreparedStatement ps) throws SQLException {
        int counter = 0;
        for (int i = 0; i < entityProps.size(); i++) {
            counter ++;
            ps.setObject(counter, entityProps.get(i).getFieldValue());
            counter ++;
            ps.setObject(counter, entityProps.get(i).getaClass().getCanonicalName());
            }
        return ps;
    }

    private SQLType getSqlTypeForPropertyClass(Class<?> propertyClass) {
        SQLType sqlType = null;
        if (String.class.equals(propertyClass)) {
            sqlType = JDBCType.VARCHAR;
        } else if (Integer.class.equals(propertyClass)) {
            sqlType = JDBCType.INTEGER;
        } else if (Double.class.equals(propertyClass)) {
            sqlType = JDBCType.NUMERIC;
        }
        return sqlType;
    }

}
