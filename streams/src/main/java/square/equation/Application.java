package square.equation;

import square.equation.calculation.NegativeDiscriminantEquation;
import square.equation.calculation.PositiveDiscriminantEquation;
import square.equation.calculation.SquareEquation;
import square.equation.calculation.ZeroDiscriminantEquation;
import square.equation.config.ConfigHolder;
import square.equation.solution.Solution;
import square.equation.solution.processing.*;
import square.equation.solution.processing.impl.ConsoleResultProcessor;
import square.equation.solution.processing.impl.FileResultProcessor;
import square.equation.solution.processing.rendering.*;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static square.equation.solution.processing.converter.ProcessTypeConverter.toProcessType;
import static square.equation.solution.processing.ProcessType.CONSOLE;
import static square.equation.solution.processing.ProcessType.FILE;

public class Application {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ConfigHolder configHolder = new ConfigHolder();

        List<SquareEquation> equations = List.of(
            new PositiveDiscriminantEquation(),
            new NegativeDiscriminantEquation(),
            new ZeroDiscriminantEquation()
        );

        Map<ProcessType, ResultProcessor> resultProcessors = Map.of(
            CONSOLE, new ConsoleResultProcessor(List.of(
                new RenderMultipleSolution(),
                new RenderSingleSolution(),
                new RenderNoneSolution()
            )),
            FILE, new FileResultProcessor(configHolder.getOutputFilePath())
        );

        final InputParameters inputParameters = new InputParameters();
        final ResultProcessor resultProcessor = resultProcessors.get(toProcessType(configHolder.getOutputMode()));

        while (true) {
            try {
                System.out.println("Enter a: ");
                inputParameters.setA(scanner.nextInt());
                System.out.println("Enter b: ");
                inputParameters.setB(scanner.nextInt());
                System.out.println("Enter c: ");
                inputParameters.setC(scanner.nextInt());
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("please enter valid value");
                continue;
            }

            double discriminant = SquareEquation.calculateDiscriminant(inputParameters.getA(), inputParameters.getB(), inputParameters.getC());

            equations.stream()
                .filter(equation -> equation.isCalculated(discriminant))
                .findFirst()
                .map(equation -> equation.calculate(inputParameters.getA(), inputParameters.getB(), discriminant))
                .ifPresent(solution -> processResult(solution, resultProcessor, inputParameters));

            if (scanner.next().equals("-q")) {
                break;
            }
        }

        resultProcessor.shutDown();
    }

    private static void processResult(Solution solution, ResultProcessor resultProcessor, InputParameters ip) {
        try {
            resultProcessor.processResult(solution, ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
