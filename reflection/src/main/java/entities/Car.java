package entities;

public class Car extends Entity {
   // @entityPropsGenerator.IgnoreField
    private final Integer speed;
    private final Double engine_volume;

    public Car(int id, Integer speed, Double engine_volume) {
        super(id);
        this.engine_volume = engine_volume;
        this.speed = speed;
    }

    public Integer getSpeed() {
        return speed;
    }

    public Double getEngineVolume() {
        return engine_volume;
    }
}



