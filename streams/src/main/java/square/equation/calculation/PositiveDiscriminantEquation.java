package square.equation.calculation;

import square.equation.solution.impl.MultipleSolution;
import square.equation.solution.Solution;

public class PositiveDiscriminantEquation extends SquareEquation {

    @Override
    public boolean isCalculated(double discriminant) {
        return discriminant > 0;
    }

    @Override
    public Solution calculate(int a, int b, double discriminant) {
        return new MultipleSolution(
            (-b + Math.sqrt(discriminant)) / (2 * a),
            (-b - Math.sqrt(discriminant)) / (2 * a)
        );
    }
}
