CREATE TABLE cats
(
    id    SERIAL PRIMARY KEY,
    breed VARCHAR,
    breed_type VARCHAR,
    name  VARCHAR,
    name_type VARCHAR,
    age VARCHAR,
    age_type VARCHAR
);