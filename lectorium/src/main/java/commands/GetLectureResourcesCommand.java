package commands;

import operations.Lecture;
import render.ResultRender;

public class GetLectureResourcesCommand implements Command {

    private final Lecture lecture;
    private final ResultRender render;

    public GetLectureResourcesCommand(Lecture lecture, ResultRender render) {
        this.lecture = lecture;
        this.render = render;
    }

    @Override
    public void execute() {
        render.renderSources(lecture.getSources());
    }

}
