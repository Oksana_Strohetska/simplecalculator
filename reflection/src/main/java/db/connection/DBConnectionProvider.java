package db.connection;

import db.connection.DBConfiguration;
import exceptions.DBConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionProvider {
        private final DBConfiguration config;

        public DBConnectionProvider(DBConfiguration config) {
            this.config = config;
        }

        public Connection getDBConnection() {
            String url = "jdbc:postgresql://" + config.getDbHost() + ":" + config.getDbPort() + "/" + config.getDbName();

            try {
                System.out.println("Try to load driver");
                Class.forName("org.postgresql.Driver");
                System.out.println("Driver is loaded successfully");
            } catch (ClassNotFoundException e) {
                throw new DBConnectionException("Class for name org.postgresql.Driver couldn't be found", e);
            }

            System.out.println("Try to get connection");
            Connection dbConnection;
            try {
                dbConnection = DriverManager.getConnection(url, config.getDbUser(), config.getDbPass());
            } catch (SQLException e) {
                throw new DBConnectionException("Connection failed", e);
            }
            System.out.println("Connection successfully");

            return dbConnection;
        }
    }
