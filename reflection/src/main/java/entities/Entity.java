package entities;

import entityPropsGenerator.IgnoreField;

public class Entity {

    @IgnoreField
    protected int id;

    public Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


}
