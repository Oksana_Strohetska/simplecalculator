package main.java.operations.imp;

import java.util.function.BinaryOperator;

public class PowOperation implements BinaryOperator<Double> {

    @Override
    public Double apply(Double aDouble, Double aDouble2) {
        return Math.pow(aDouble, aDouble2);
    }
}
