package user.sort;

import user.dto.User;

import java.util.Comparator;

public interface Sort {
    Comparator<User> getComparator();
}

