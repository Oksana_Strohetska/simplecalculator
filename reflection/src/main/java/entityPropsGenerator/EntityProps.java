package entityPropsGenerator;

public class EntityProps<T> {
        private final String fieldName;
        private final String fieldValue;
        private final Class aClass;

        public EntityProps(String fieldName, String fieldValue, Class aClass) {
            this.aClass = aClass;
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
        }

        public String getFieldName() {
            return fieldName;
        }

        public String getFieldValue() {
            return fieldValue;
        }

        public Class getaClass() { return aClass; }

        @Override
        public String toString() {
            return "EntityProps{" +
                    "fieldName='" + getFieldName() + '\'' +
                    ", fieldValue='" + getFieldValue() + '\'' +
                    '}';
        }


}
