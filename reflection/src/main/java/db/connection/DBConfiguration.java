package db.connection;

import exceptions.DBConnectionException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfiguration {
        public static final String PROPERTIES_PATH = "reflection/src/main/resources/configDB.properties";
        public final Properties props = new Properties();

        public DBConfiguration() {
            try (FileInputStream fis = new FileInputStream(PROPERTIES_PATH)) {
                this.props.load(fis);
            } catch (IOException e) {
                throw new DBConnectionException("DB configs are not loaded", e);
            }
        }
        public String getDbHost() {
            return this.props.getProperty("db.host");
        }
        public String getDbPort() { return this.props.getProperty("db.port"); }
        public String getDbName() { return this.props.getProperty("db.name"); }
        public String getDbUser() {
            return this.props.getProperty("db.user");
        }
        public String getDbPass() { return this.props.getProperty("db.password"); }
    }
