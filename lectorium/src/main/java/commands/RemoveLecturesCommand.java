package commands;

import operations.Library;
import render.ResultRender;

import java.util.Scanner;

public class RemoveLecturesCommand implements Command {
    private final Library library;
    private final ResultRender render;
    private final Scanner scanner;
    public RemoveLecturesCommand(Library library, ResultRender render, Scanner scanner) {
        this.library = library;
        this.render = render;
        this.scanner = scanner;
    }

    @Override
    public void execute() {
        render.render(library.removeLecture(scanner.nextInt()));
    }

}
