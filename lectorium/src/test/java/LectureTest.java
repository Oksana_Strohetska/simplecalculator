import operations.Lecture;
import operations.Resources;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LectureTest {

    @Test
    void getLectureName() {
        Resources r1 = new Resources("Doc1");
        Resources r2 = new Resources("Doc2");
        Lecture lecture = new Lecture("LectureName", List.of(r1, r2));
        String expected = "LectureName";
        String actual = lecture.getLectureName();
        assertEquals(actual, expected);
    }

    @Test
    void addSources() {
        Resources r1 = new Resources("Doc1");
        Resources r2 = new Resources("Doc2");
        Resources r3 = new Resources("Doc3");
        Lecture lecture = new Lecture("LectureName", new ArrayList<>(Arrays.asList(r1, r2)));
        List<Resources> addingSource = List.of(r3);
        lecture.addSources(addingSource);
        addingSource.forEach(source -> assertTrue(lecture.getSources().contains(source)));
    }

    @Test
    void removeSources() {
        Resources r1 = new Resources("Doc1");
        Resources r2 = new Resources("Doc2");
        Lecture lecture = new Lecture("LectureName", new ArrayList<>(Arrays.asList(r1, r2)));
        lecture.removeSources(List.of(r1));
        assertFalse(lecture.getSources().contains(r1));
    }

    @Test
    void getSources() {
        Resources r1 = new Resources("Doc1");
        Resources r2 = new Resources("Doc2");
        Resources r3 = new Resources("Doc3");
        Lecture lecture = new Lecture("LectureName", new ArrayList<>(Arrays.asList(r1, r2, r3)));
        List<Resources> expected = List.of(r1, r2, r3);
        List<Resources> actual = lecture.getSources();
        assertEquals(actual, expected);
    }
}