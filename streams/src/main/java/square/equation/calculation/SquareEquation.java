package square.equation.calculation;

import square.equation.solution.Solution;

public abstract class SquareEquation {

    public abstract boolean isCalculated(double discriminant);

    public abstract Solution calculate(int a, int b, double discriminant);

    public static double calculateDiscriminant(int a, int b, int c) {
        return b * b - 4 * a * c;
    }

}
