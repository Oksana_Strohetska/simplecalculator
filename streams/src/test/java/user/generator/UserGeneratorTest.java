package user.generator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import user.dto.User;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserGeneratorTest {
    private UserGeneratorImpl userGenerator;

    @BeforeEach
    void setUp() {
        userGenerator = new UserGeneratorImpl();
    }

    @Test
    void generate_user_should_return_generated_user () {
        User user = userGenerator.generateUser();
        assertUser(user);
    }

    private void assertUser (User user) {
        assertNotNull(user);
        assertNotNull(user.getFirstName());
        assertNotNull(user.getLastName());
        assertNotNull(user.getBirthday());
    }

    @Test
    void generate_users_should_return_list_of_users_by_specified_size() {
        List<User> actual = userGenerator.generateUsers(5);
        assertEquals(5, actual.size());
        for (User user: actual) {
            assertUser(user);
        }
        System.out.println(actual.stream().map(User::getBirthday));
    }
}
