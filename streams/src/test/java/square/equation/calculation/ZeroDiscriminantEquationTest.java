package square.equation.calculation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import square.equation.solution.Solution;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ZeroDiscriminantEquationTest {

    ZeroDiscriminantEquation zeroDiscriminantEquation;

    @BeforeEach
    void setUp() {
        zeroDiscriminantEquation= new ZeroDiscriminantEquation();
    }

    @Test
    void zeroDiscriminantEquation_should_calculate_result_for_zero_discriminant() {
        assertTrue(zeroDiscriminantEquation.isCalculated(0));
        assertFalse(zeroDiscriminantEquation.isCalculated(1));
        assertFalse(zeroDiscriminantEquation.isCalculated(-1));
    }

    @ParameterizedTest
    @ArgumentsSource(ZeroDiscriminantArgumentsProvider.class)
    void test_calculate_square_equation_with_zero_discriminant(
            Integer a,
            Integer b,
            Double discriminant,
            Double x1
    ) {
        Solution actual = zeroDiscriminantEquation.calculate(a, b, discriminant);
        assertNotNull(actual.getX1());
        assertEquals(actual.getX1(), x1);
        assertNull(actual.getX2());
    }

    private static class ZeroDiscriminantArgumentsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    Arguments.of(1, 2, 0., -1.0),
                    Arguments.of(-1, -2, 0., -1.0),
                    Arguments.of(2, 0, 0., 0.)
            );
        }
    }
}
