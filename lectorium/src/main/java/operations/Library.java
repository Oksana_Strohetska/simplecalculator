package operations;

import java.util.ArrayList;
import java.util.List;

public class Library{
    private List<Lecture> lectures = new ArrayList<>();

    public List<Lecture> getLectures() {
        return lectures;
    }

    public List<Lecture> getAllLectures() {
        return lectures;
    }

    public void addLecture(Lecture newLecture){
        lectures.add(newLecture);
    }

    public void addLecture(Lecture newLecture, List<Resources> source) {
        lectures.add(newLecture);
        int addedLectureIndex = lectures.size()-1;
        lectures.get(addedLectureIndex).addSources(source);
    }

    public Lecture removeLecture(int id) {
       return lectures.remove(id);
    }

    public Lecture getLecture(int id) {
        Lecture lecture = lectures.get(id);
        return lecture;
    }

    static void quit() {
        System.out.println("Lectorium is closed");
    }
}
