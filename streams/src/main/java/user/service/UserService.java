package user.service;

import user.dto.User;
import user.sort.Sort;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Function;

public interface UserService {
    List<User> generate(int number);

    long save(List<User> users);

    List<User> getAll();

    User oldestUser();

    Map<Integer, List<User>> getAgeUserGroup();

    List<User> search(Predicate<User> userFilter);

    List<User> sort(Sort sort);

    List<User> multipleSort(List<Sort> sort);
}
