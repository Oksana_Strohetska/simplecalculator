package user.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import user.dto.User;
import user.generator.UserGenerator;
import user.repository.UserRepository;
import user.sort.Asc;
import user.sort.Desc;
import user.sort.Sort;
import user.sort.SortType;
import user.sort.map.SortedParams;
import utils.DateProvider;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static simple.UserFactory.createUser;

class UserServiceImplTest {
    private UserServiceImpl userService;
    DateProvider dateProvider;

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserGenerator userGenerator;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        dateProvider = new DateProvider();
        userService = new UserServiceImpl(userGenerator, userRepository, dateProvider);
    }

    @Test
    void oldestUser_should_return_oldest_user() {
        //given
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("baz", "boo", true,
                LocalDate.of(1994, 11, 5)),
            createUser("Bob", "boo", false,
                LocalDate.of(2000, 1, 12))
        );

        //when
        when(userRepository.getAll()).thenReturn(users);

        User expected = createUser("baz", "boo", true,
            LocalDate.of(1994, 11, 5));
        User actual = userService.oldestUser();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void oldestUser_should_return_null_if_no_users() {
        //when
        when(userRepository.getAll()).thenReturn(List.of());

        User actual = userService.oldestUser();

        //then
        assertNull(actual);
    }

    @Test
    void save_should_return_count_of_saved_users() {
        //given
        List<User> users = List.of(
            createUser("foo", false),
            createUser("bar", true),
            createUser("baz", false)
        );

        userService.save(users);

        verify(userRepository, times(users.size())).add(userArgumentCaptor.capture());

        List<User> capturedValues = userArgumentCaptor.getAllValues();

        assertEquals(users, capturedValues);
    }

    @Test
    void search_should_return_filtered_users() {
        //given
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1994, 11, 5)),
            createUser("Bob", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        //when
        when(userRepository.getAll()).thenReturn(users);

        List<User> actual = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1994, 11, 5))
        );
        List<User> expected = userService.search(user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday()) > 10);

        //then
        assertEquals(actual, expected);
    }

    @Test
    void search_should_return_empty_list_if_no_filtered_users() {
        //given
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Bob", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        //when
        when(userRepository.getAll()).thenReturn(users);

        List<User> actual = userService.search(user -> user.getBirthday().getYear() > 30);

        //then
        assertTrue(actual.isEmpty());
    }

    @Test
    void sort_should_return_user_list_sorted_by_first_name_asc() {
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        when(userRepository.getAll()).thenReturn(users);

        List<User> expected = List.of(
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)));
        List<User> actual = userService.multipleSort(List.of(new Asc(User::getFirstName)));

        assertEquals(expected, actual);
    }

    @Test
    void sort_should_return_user_list_sorted_by_birthday_desc() {
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        when(userRepository.getAll()).thenReturn(users);

        List<User> expected = List.of(
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12)),
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)));

        List<User> actual = userService.multipleSort(List.of(new Desc(User::getBirthday)));

        assertEquals(actual, expected);
    }

    @Test
    void sort_should_return_empty_list_when_sorting_empty_user_list_by_id_asc() {
        List<User> users = new ArrayList<>();

        when(userRepository.getAll()).thenReturn(users);

        assertTrue(userService.multipleSort(List.of(new Asc(User::getId))).isEmpty());
    }

    @Test
    void sort_should_return_user_list_sorted_by_last_name_desc() {
        List<User> users = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        when(userRepository.getAll()).thenReturn(users);

        List<User> expected = List.of(
            createUser("Bob", "boo", false,
                LocalDate.of(2009, 1, 12)),
            createUser("Bill", "boo", true,
                LocalDate.of(1984, 11, 5)),
            createUser("Ben", "boo", false,
                LocalDate.of(2010, 10, 12))
        );

        List<User> actual = userService.multipleSort(List.of(new Desc(User::getLastName)));

        assertEquals(actual, expected);
    }

    @Test
    void multiple_sort_should_return_user_list_sorted_by_married_false_and_first_name_asc() {
        List<User> users = List.of(
                createUser("Ben", "boo", false,
                        LocalDate.of(2010, 10, 12)),
                createUser("Bill", "boo", true,
                        LocalDate.of(1984, 11, 5)),
                createUser("Bob", "boo", false,
                        LocalDate.of(2009, 1, 12))
        );

        when(userRepository.getAll()).thenReturn(users);

        List<User> expected = List.of(
                createUser("Ben", "boo", false,
                        LocalDate.of(2010, 10, 12)),
                createUser("Bob", "boo", false,
                        LocalDate.of(2009, 1, 12)),
                createUser("Bill", "boo", true,
                        LocalDate.of(1984, 11, 5))
        );

        List<Sort> sorts = List.of(
            new Asc(User::isMarried),
            new Asc(User::getFirstName)
        );

        List<User> actual = userService.multipleSort(sorts);

        assertIterableEquals(expected, actual);
    }

    @Test
    void multiple_sort_should_return_user_list_sorted_by_birthday_desc_and_first_name_asc() {
        List<User> users = List.of(
            createUser("Rob", LocalDate.of(2009, 1, 12)),
            createUser("Bill", LocalDate.of(1984, 11, 5)),
            createUser("Bob", LocalDate.of(2009, 1, 12))
        );

        when(userRepository.getAll()).thenReturn(users);

        List<User> expected = List.of(
            createUser("Bob", LocalDate.of(2009, 1, 12)),
            createUser("Rob", LocalDate.of(2009, 1, 12)),
            createUser("Bill", LocalDate.of(1984, 11, 5))
        );

        List<User> actual = userService.multiSortRefactoring(List.of(
                new SortedParams(SortType.DESC, User::getBirthday),
                new SortedParams(SortType.ASC, User::getFirstName)));

        assertIterableEquals(expected, actual);
    }
}