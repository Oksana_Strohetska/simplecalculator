package entities;

public class Animal extends Entity {

    protected String breed;

    public Animal(int id, String breed) {
        super(id);
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }
}
