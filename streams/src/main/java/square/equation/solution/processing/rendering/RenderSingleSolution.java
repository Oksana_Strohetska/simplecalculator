package square.equation.solution.processing.rendering;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class RenderSingleSolution implements Render {

    @Override
    public boolean canRender (Solution solution) {
        return solution.getResultType() == ResultType.SINGLE;
    }

    @Override
    public void renderResults(Solution solution) {
        System.out.println("x1 is: " + solution.getX1());
    }
}