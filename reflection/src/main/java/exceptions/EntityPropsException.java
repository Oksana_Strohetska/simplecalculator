package exceptions;

public class EntityPropsException extends RuntimeException {
    public EntityPropsException(String message, Throwable cause) {
        super(message, cause);
    }
}
