CREATE TABLE sousages
(
    id     SERIAL PRIMARY KEY,
    weight VARCHAR,
    weight_type VARCHAR,
    length VARCHAR,
    length_type VARCHAR
);