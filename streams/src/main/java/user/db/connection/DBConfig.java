package user.db.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfig {
    public static final String PROPERTIES_PATH = "streams/src/main/resources/config.properties";
    public final Properties props = new Properties();

    public DBConfig() {
        try (FileInputStream fis = new FileInputStream(PROPERTIES_PATH)) {
            this.props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getDbHost() {
        return this.props.getProperty("db.host");
    }
    public String getDbPort() { return this.props.getProperty("db.port"); }
    public String getDbName() { return this.props.getProperty("db.name"); }
    public String getDbUser() {
        return this.props.getProperty("db.user");
    }
    public String getDbPass() { return this.props.getProperty("db.password"); }
}
