package square.equation.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigHolder {
    private static final String PROPERTIES_PATH = "streams/src/main/resources/config.properties";

    private final Properties properties = new Properties();

    public ConfigHolder() {
        try (FileInputStream fis = new FileInputStream(PROPERTIES_PATH)) {
            this.properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getOutputMode() {
        return this.properties.getProperty("output.mode");
    }

    public String getOutputFilePath() {
        return this.properties.getProperty("output.file.path");
    }
}
