import operations.Lecture;
import operations.Library;
import operations.Resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Console {
    public final Scanner scanner;
    public final Library library;

    public Console(Library library) {
        this.library = library;
        this.scanner = new Scanner(System.in);
    }

    void performFirstLevelOperations(Library library) {
        System.out.println("Select Operation: \n" +
                "1 - Get all Lectures \n" +
                "2 - Add new Lecture \n" +
                "3 - Remove Lecture by its number \n" +
                "4 - Get Lectures by numbers \n" +
                "-1 - Exit");

        while (true) {
            int operationNumber = readNumber(scanner);
            switch (operationNumber) {
                case (1):
                    System.out.println("All available lectures in the library:\n");
                    List<Lecture> lectures = library.getAllLectures();
                    for (int i = 0; i < lectures.size(); i++) {
                        int j = i+1;
                        System.out.println(j + " " + lectures.get(i).getLectureName());
                    }
                    continue;
                case (2):
                    System.out.println("Type in the new lecture name \n");
                    String lectureName = readData(scanner);
                    System.out.println("Type in the resource titles, separated by coma or 0 if no resources");
                    String resourcesToAdd = readData(scanner);
                    if (resourcesToAdd.equals("0")) {
                        Lecture lecture = new Lecture(lectureName, Collections.emptyList());
                        library.addLecture(lecture);
                    } else {
                        List<Resources> resourcesList = Arrays.stream(resourcesToAdd.split(","))
                                .map(Resources::new)
                                .collect(Collectors.toList());
                        library.addLecture(new Lecture(lectureName, resourcesList));
                    }
                    System.out.printf("Lecture %s was added successfully%n \n", lectureName);
                    continue;
                case (3):
                    System.out.println("Please type in the lecture number to remove \n");
                    int number = readNumber(scanner);
                    if (number > library.getLectures().size()) {
                        System.out.printf("There is no available lecture with %d index. Try again \n", number);
                    } else {
                        library.removeLecture(number - 1);
                        System.out.printf("Lecture with number %d was removed successfully%n \n", number);
                    }
                    continue;
                case (4):
                    System.out.println("Type in the lecture numbers separated by coma \n");
                    String nums = readData(scanner);
                    List<Integer> numbers = Arrays.stream(nums.split(","))
                            .map(Integer::parseInt)
                            .collect(Collectors.toList());
                    for (Integer id : numbers) {
                        if (id > library.getLectures().size()) {
                            System.out.printf("There is no available lecture with %d index \n", id);
                        } else {
                            Lecture searchedLecture = library.getLecture(id - 1);
                            System.out.println(searchedLecture.toString() + "\n");
                        }
                    }
                    System.out.println("Type in the lecture number for working on lecture \n");
                    int id = readNumber(scanner);
                    performSecondLevelOperations(library.getLecture(id -1));
                    continue;
                case (-1):
                    return;
                default:
                    System.out.println("There is no available operation. Try again");
            }
        } 
    }

    void performSecondLevelOperations(Lecture lecture) {
        System.out.println("Select Operation: \n"+
                "1 - Get List of content \n" +
                "2 - Add new content \n" +
                "3 - Remove content \n" +
                "4 - Go to the First Level Operations");
        while (true) {
            switch (readNumber(scanner)) {
                case (1):
                    lecture.getSources();
                    continue;
                case (2):
                    System.out.println("Type in the content name/link to add \n");
                    String newContent = readData(scanner);
                    lecture.addSources(List.of(new Resources(newContent)));
                    System.out.printf("The content %s was added to lecture successfully \n", newContent);
                    continue;
                case (3):
                    System.out.println("Type in the content/link to remove \n");
                    String remContent = readData(scanner);
                    lecture.removeSources(List.of(new Resources(remContent)));
                    continue;
                case (4):
                    performFirstLevelOperations(library);
                    return;
                default:
                    System.out.println("There are no available operations by entered value");
            }
        }
    }

    String readData (Scanner scanner) {
        return scanner.next();
    }

    Integer readNumber (Scanner scanner) {
        return scanner.nextInt();
    }
}
