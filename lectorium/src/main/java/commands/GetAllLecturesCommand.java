package commands;

import operations.Library;
import render.ResultRender;

public class GetAllLecturesCommand implements Command {
    private final Library library;
    private final ResultRender render;

    public GetAllLecturesCommand(Library library, ResultRender render) {
        this.library = library;
        this.render = render;
    }

    @Override
    public void execute() {
    render.render(library.getLectures());
    }
}
