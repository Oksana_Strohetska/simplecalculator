package square.equation.solution.processing.impl;

import square.equation.solution.Solution;
import square.equation.solution.processing.ResultProcessor;
import square.equation.solution.processing.rendering.InputParameters;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import static java.nio.charset.StandardCharsets.UTF_8;

public class FileResultProcessor implements ResultProcessor {
    private Writer writer;

    public FileResultProcessor(String file) {
        try {
            this.writer = new OutputStreamWriter(new FileOutputStream(file, true), UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void shutDown() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processResult(Solution solution, InputParameters inputParameters){
        ReportResultBuilder rb = ReportResultBuilder.builder()
            .addInputParameters(inputParameters)
            .addSolution(solution);

        try {
            this.writer.write(rb.buildReport());
            this.writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
