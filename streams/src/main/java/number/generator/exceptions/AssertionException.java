package number.generator.exceptions;

public class AssertionException extends RuntimeException {
    public AssertionException(String message) {
        super(message);
    }
}
