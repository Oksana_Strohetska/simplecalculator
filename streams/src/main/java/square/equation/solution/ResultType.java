package square.equation.solution;

public enum ResultType {
    NONE,
    MULTIPLE,
    SINGLE
}
