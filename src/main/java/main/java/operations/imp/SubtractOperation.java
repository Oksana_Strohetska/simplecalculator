package main.java.operations.imp;

import java.util.function.BinaryOperator;

public class SubtractOperation implements BinaryOperator<Double> {

    @Override
    public Double apply(Double aDouble, Double aDouble2) {
        return aDouble - aDouble2;
    }
}
