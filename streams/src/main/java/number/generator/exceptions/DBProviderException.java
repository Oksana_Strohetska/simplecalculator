package number.generator.exceptions;

public class DBProviderException extends RuntimeException {
    public DBProviderException(String message, Throwable cause) {
        super(message, cause);
    }
}
