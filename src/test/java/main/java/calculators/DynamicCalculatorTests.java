package main.java.calculators;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DynamicCalculatorTests {

    private Map<String, BinaryOperator<Double>> availableOperations = new HashMap<>();

    @Test
    public void testSumOperation() {
        BinaryOperator<Double> selectedOperation = availableOperations.get("+");
        assertEquals(selectedOperation.apply(3.6, 5.8), 9.4, 0.01);
    }

    @Test
    public void testSubtractOperation() {
        BinaryOperator<Double> selectedOperation = availableOperations.get("-");
        assertEquals(selectedOperation.apply(9.6, 5.8), 3.8, 0.01);
    }

    @Test
    public void testMultiplyOperation() {
        BinaryOperator<Double> selectedOperation = availableOperations.get("*");
        assertEquals(selectedOperation.apply(3.6, 2.0), 7.2, 0.01);
    }

    @Test
    public void testDivideOperation() {
        BinaryOperator<Double> selectedOperation = availableOperations.get("/");
        assertEquals(selectedOperation.apply(9.6, 3.0), 3.2, 0.01);
    }

    @Test
    public void testPowOperation() {
        BinaryOperator<Double> selectedOperation = availableOperations.get("^");
        assertEquals(selectedOperation.apply(2.0, 2.0), 4.0, 0.01);
    }

}
