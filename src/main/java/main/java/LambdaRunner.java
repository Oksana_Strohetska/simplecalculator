package main.java;

import main.java.calculators.Calculator;
import main.java.calculators.impl.DynamicCalculatorImp;

import java.util.Scanner;
import java.util.regex.Pattern;

public class LambdaRunner {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Calculator calculator = DynamicCalculatorImp.builder()
                .addOperation("+", (firstNumber, secondNumber) -> firstNumber + secondNumber)
                .addOperation("-", (a, b) -> a - b)
                .addOperation("*", (Double a, Double b) -> a * b)
                .addOperation("/", (a,  b) -> a / b )
                .addOperation("^", (x, y) -> Math.pow(x, y))
                .build();


        String input;
        while (!(input = scanner.nextLine()).equals("--exit")) {
            if (input.equals("--help")) {
                System.out.println(calculator.getAvailableOperations());
                continue;
            }
            System.out.print("-->");
            //todo add simply input validation
            //todo adjust this impementation
            Pattern pattern = Pattern.compile("^\\d*\\.?\\d*\\s"
                    + calculator.getAvailableOperations().toString()
                    + "\\s\\d*\\.?\\d*\\$");
            if (pattern.matcher(input).find()) {
                System.out.println("Wrong input. Please try again.");
                continue;
            }
            String[] inputArguments = input.split(" ");
            Double firstNumber = Double.valueOf(inputArguments[0]);
            String operator = inputArguments[1];
            Double secondNumber = Double.valueOf(inputArguments[2]);
            System.out.println("-->" +  calculator.doOperation(operator, firstNumber, secondNumber));
        }
    }
}
