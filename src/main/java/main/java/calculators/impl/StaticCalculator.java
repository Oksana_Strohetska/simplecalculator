package main.java.calculators.impl;

import main.java.calculators.Calculator;

import java.util.Set;
import java.util.function.BinaryOperator;

public class StaticCalculator implements Calculator {

    private BinaryOperator<Double> sumOperation
            , subtractOperation
            , multiplyOperation
            , divideOperation;

    public StaticCalculator(
            BinaryOperator<Double> sumOperation,
            BinaryOperator<Double> subtractOperation,
            BinaryOperator<Double> multiplyOperation,
            BinaryOperator<Double> divideOperation
    ) {
        this.sumOperation = sumOperation;
        this.subtractOperation = subtractOperation;
        this.multiplyOperation = multiplyOperation;
        this.divideOperation = divideOperation;
    }

    @Override
    public Double doOperation(String operator, Double firstNumber, Double secondNumber) {
        switch (operator) {
            case "+":
                return sumOperation.apply(firstNumber, secondNumber);
            case "-":
                return subtractOperation.apply(firstNumber, secondNumber);
            case "*":
                return multiplyOperation.apply(firstNumber, secondNumber);
            case "/":
                return divideOperation.apply(firstNumber, secondNumber);
            default:
                throw new IllegalArgumentException("Unsupported operation");
        }
    }

    @Override
    public Set<String> getAvailableOperations() {

        return Set.of("* / + -");
    }
}
