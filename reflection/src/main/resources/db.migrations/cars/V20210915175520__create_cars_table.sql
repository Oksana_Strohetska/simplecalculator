create table cars
(
    id SERIAL PRIMARY KEY,
    engine_volume VARCHAR,
    engine_volume_type VARCHAR,
    speed VARCHAR,
    speed_type VARCHAR
);