package user.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import user.dto.User;
import utils.DateProvider;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static simple.UserFactory.createUser;

public class UserRepositoryTests {
    private UserRepositoryInMemory userRepositoryInMemory;
    private DateProvider dateProvider;

    @BeforeEach
    void setUp(){
        userRepositoryInMemory = new UserRepositoryInMemory();
        dateProvider = new DateProvider();
    }

    @Test
    void add_should_add_users_to_repository_and_assign_ids() {
        User user1 = new User();
        User user2 = new User();

        User first = userRepositoryInMemory.add(user1);
        User second = userRepositoryInMemory.add(user2);

        List<Integer> expectedUsersIds = Stream.of(first, second)
                .map(User::getId)
                .collect(Collectors.toList());

        List<User> allUsers = userRepositoryInMemory.getAll();
        List<Integer> actualUserIds = allUsers.stream()
                .map(User::getId)
                .collect(Collectors.toList());

        assertIterableEquals(expectedUsersIds, actualUserIds);
    }

    @Test
    void get_should_return_user_by_id() {
        User first = createUser("Bob", LocalDate.of(2010, 1, 12));
        User second = createUser("Bill", LocalDate.of(1995, 5, 26));

        User firstUser = userRepositoryInMemory.add(first);
        User secondUser = userRepositoryInMemory.add(second);

        List<User> expected = List.of(first, second);
        List<User> actual = List.of(firstUser, secondUser);

        assertIterableEquals(expected.stream().map(User::getFirstName).collect(Collectors.toList()),
                actual.stream().map(User::getFirstName).collect(Collectors.toList()));
        assertIterableEquals(expected.stream().map(user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())).collect(Collectors.toList()),
                actual.stream().map(user -> dateProvider.calculateWholeYearsCountFromPeriod(user.getBirthday())).collect(Collectors.toList()));
        assertIterableEquals(expected.stream().map(User::isMarried).collect(Collectors.toList()),
                actual.stream().map(User::isMarried).collect(Collectors.toList()));
    }

    @Test
    void delete_should_remove_user_from_repository() {
       userRepositoryInMemory.add(createUser("Bob", LocalDate.of(2010, 1, 12)));
       userRepositoryInMemory.add(createUser("Bill", LocalDate.of(1995, 5, 26)));

       assertFalse(userRepositoryInMemory.getAll().isEmpty());

        for (User user : userRepositoryInMemory.getAll()) {
            assertTrue(userRepositoryInMemory.delete(user));
        }

        assertTrue(userRepositoryInMemory.getAll().isEmpty());
        assertFalse(userRepositoryInMemory.delete(new User()));
   }

   @Test
    void getAll_should_return_all_users_from_repository() {
       User first = createUser("Bob", LocalDate.of(2010, 1, 12));
       User second = createUser("Bill", LocalDate.of(1995, 5, 26));
       userRepositoryInMemory.add(first);
       userRepositoryInMemory.add(second);

       assertFalse(userRepositoryInMemory.getAll().isEmpty());
       assertEquals(userRepositoryInMemory.getAll(), List.of(first, second));
   }

    @Test
    void update_should_update_user_in_repository_and_return_updated_user() {
        User newUser = createUser("Foo",LocalDate.of(2004, 1, 12));
        assertNull(newUser.getId());

        User addedUser = userRepositoryInMemory.add(newUser);
        assertNotNull(addedUser.getId());

        addedUser.setMarried(true);

        User updatedUser = userRepositoryInMemory.update(addedUser);
        assertEquals(updatedUser.getId(), addedUser.getId());
        assertTrue(updatedUser.isMarried());

        assertNull(userRepositoryInMemory.update(new User()));
    }

    @Test
    void deleteAll_should_remove_all_users_in_repository() {
        userRepositoryInMemory.add(createUser("Bob", LocalDate.of(2010, 1, 12)));
        userRepositoryInMemory.add(createUser("Bill", LocalDate.of(1995, 5, 26)));

        assertFalse(userRepositoryInMemory.getAll().isEmpty());

        userRepositoryInMemory.deleteAll();

        assertTrue(userRepositoryInMemory.getAll().isEmpty());
    }

    @AfterEach
    void tearDown() {
        userRepositoryInMemory.deleteAll();
    }
}
