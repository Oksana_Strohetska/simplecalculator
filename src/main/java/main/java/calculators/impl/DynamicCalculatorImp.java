package main.java.calculators.impl;

import main.java.calculators.Calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BinaryOperator;

public class DynamicCalculatorImp implements Calculator {
    private Map<String, BinaryOperator<Double>> availableOperations = new HashMap<>();

    private DynamicCalculatorImp(Map<String, BinaryOperator<Double>> availableOperations) {
        this.availableOperations.putAll(availableOperations);
    }

    @Override
    public Double doOperation(String operator, Double firstNumber, Double secondNumber) {
        BinaryOperator<Double> selectedOperation = availableOperations.get(operator);
        return selectedOperation.apply(firstNumber, secondNumber);
    }

    @Override
    public Set<String> getAvailableOperations() {
        return availableOperations.keySet();
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {
        private Map<String, BinaryOperator<Double>> availableOperations = new HashMap<>();

        public Builder addOperation(String operator, BinaryOperator<Double> operation) {
            this.availableOperations.put(operator, operation);
            return this;
        }
        
        public DynamicCalculatorImp build() {
            return new DynamicCalculatorImp(availableOperations);
        }
    }
}