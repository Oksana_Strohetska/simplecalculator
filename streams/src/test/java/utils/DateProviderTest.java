package utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateProviderTest {

    private DateProvider dateProvider = new DateProvider();

    @Test
    public void getRandomNumberInRange_should_return_int_within_range() {
        int min = 10;
        int max = 20;
        int actualNumber = dateProvider.getRandomNumberInRange(min, max);
        assertTrue(actualNumber >= min);
        assertTrue(actualNumber <= max);
    }

    @Test
    public void generateRandomDate_should_return_date_before_today() {
        LocalDate actual = dateProvider.generateRandomDate();
        assertTrue(actual.isBefore(LocalDate.now()));
    }

    @Test
    public void calculateWholeYearsCountFromPeriod_should_return_the_whole_years_amount_before_today() {
        int expected = 5;
        int actual = dateProvider.calculateWholeYearsCountFromPeriod(LocalDate.of(2014, 3,25));
        assertEquals(expected, actual);
    }
}
