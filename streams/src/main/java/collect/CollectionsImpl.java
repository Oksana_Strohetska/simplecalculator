package collect;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class CollectionsImpl implements Collections {

    @Override
    public <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> resultList = new ArrayList<>();
        for (E e : elements) {
            if (filter.test(e)) {
                resultList.add(e);
            }
        }
        return resultList;
    }

    @Override
    public <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return !allMatch(elements, predicate);
    }

    @Override
    public <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction) {
        List<R> results = new ArrayList<>();
        for (T element : elements) {
            results.add(mappingFunction.apply(element));
        }
        return results;
    }

    @Override
    public <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    @Override
    public <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }

        E max = elements.get(0);
        for (E element : elements) {
            if (comparator.compare(element, max) > 0) {
                max = element;
            }
        }

        return Optional.of(max);
    }

    @Override
    public <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return this.max(elements, comparator.reversed());
    }

    @Override
    public <E> List<E> distinct(List<E> elements) {
        Set<E> set = new HashSet<>(elements);
        return new ArrayList<>(set);
    }

    @Override
    public <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        Iterator<E> iterator = elements.iterator();
        E reducedValue = iterator.next();

        while (iterator.hasNext()) {
            reducedValue = accumulator.apply(reducedValue, iterator.next());
        }
        return Optional.of(reducedValue);
    }

    @Override
    public <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        for (E element : elements) {
            seed = accumulator.apply(seed, element);
        }
        return seed;
    }

    @Override
    public <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate){
        Map <Boolean, List<E>> resultMap = new HashMap<>();
        List<E> trueList = new ArrayList<>();
        List<E> falseList = new ArrayList<>();
        for (E element : elements) {
            if (predicate.test(element)) {
                trueList.add(element);
            } else {
                falseList.add(element);
            }
        }
        resultMap.put(true, trueList);
        resultMap.put(false, falseList);
        return resultMap;
    }

    @Override
    public <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> result = new HashMap<>();
        if (!elements.isEmpty()) {
            for (T element : elements) {
                K key = classifier.apply(element);
                if (result.containsKey(key)) {
                    List<T> values = result.get(key);
                    values.add(element);
                    result.replace(key, values);
                } else {
                    List<T> values = new ArrayList<>();
                    values.add(element);
                    result.put(key, values);
                }
            }
        }
        return result;
    }

    @Override
    public <T, K, U> Map<K, U> toMap(
            List<T> elements,
            Function<T, K> keyFunction,
            Function<T, U> valueFunction,
            BinaryOperator<U> mergeFunction) {
        Map<K, U> result = new HashMap<>();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            U value = valueFunction.apply(element);
            if (result.containsKey(key)) {
                result.replace(
                        key,
                        mergeFunction.apply(result.get(key), value)
                );
            }
            else {
            result.put(key, value);
            }
        }
        return result;
    }
}
