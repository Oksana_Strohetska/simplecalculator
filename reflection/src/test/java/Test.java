import entities.Animal;
import entities.Cat;
import entities.Entity;
import entityPropsGenerator.EntityProps;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static entityPropsGenerator.EntityPropsGenerator.getEntityProps;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RecursionTest {
    Entity cat1 = new Cat(1, "Sphynx", "foo", 10);
    Animal cat2 = new Cat(2, "Seam", "Tom", 2);
    Cat cat3 = new Cat(3, "Pers", "bar", 2);


    @Test
    public void testRecursionMethod() {
        List<Field> catFields = Arrays.asList(cat1.getClass().getDeclaredFields());
        List<EntityProps> allCatFields = getEntityProps(cat1);
        assertNotEquals(catFields.size(), allCatFields.size());
        assertTrue(allCatFields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("id")));
        assertTrue(allCatFields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("breed")));

        List<Field> cat2Fields = Arrays.asList(cat2.getClass().getDeclaredFields());
        List<EntityProps> allCat2Fields = getEntityProps(cat2);
        assertNotEquals(cat2Fields.size(), allCat2Fields.size());
        assertEquals(allCat2Fields.size(), allCatFields.size());
        assertTrue(allCat2Fields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("id")));
        assertTrue(allCat2Fields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("breed")));

        List<Field> cat3Fields = Arrays.asList(cat3.getClass().getDeclaredFields());
        List<EntityProps> allCat3Fields = getEntityProps(cat3);
        assertNotEquals(cat3Fields.size(), allCat3Fields.size());
        assertEquals(allCat2Fields.size(), allCat3Fields.size());
        assertTrue(allCat3Fields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("id")));
        assertTrue(allCat3Fields.stream().map(EntityProps::getFieldName)
            .anyMatch(fieldName -> fieldName.equals("breed")));
    }
}
