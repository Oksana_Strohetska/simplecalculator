package user.generator;

import user.dto.User;

import java.util.List;

public interface UserGenerator {
    User generateUser();

    List<User> generateUsers(int count);
}
