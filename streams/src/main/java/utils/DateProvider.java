package utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.concurrent.ThreadLocalRandom;

public class DateProvider {

    public LocalDate generateRandomDate(int min, int max) {
        int intYear = getRandomNumberInRange(min, max);
        Year year = Year.of(intYear);
        if (year.equals(Year.now())) {
            return year.atDay(getRandomNumberInRange(1, LocalDate.now().getDayOfYear()));
        }

        int dayOfYear = getRandomNumberInRange(1, LocalDate.of(intYear, 1, 1)
                .with(TemporalAdjusters.lastDayOfYear()).getDayOfYear());
        return year.atDay(dayOfYear);
    }

    public LocalDate generateRandomDate() {
        return generateRandomDate(1960, LocalDate.now().getYear());
    }

    public int calculateWholeYearsCountFromPeriod(LocalDate date) {
        return Period.between(date, LocalDate.now()).getYears();
    }

    int getRandomNumberInRange(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }
}
