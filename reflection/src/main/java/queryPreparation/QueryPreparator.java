package queryPreparation;

import entities.Entity;
import entityPropsGenerator.EntityProps;

import java.util.List;

public class QueryPreparator {

    Entity entity;

    public QueryPreparator(Entity entity) {
        this.entity = entity;
    }

    public String insertQueryPreparing(String tableName, List<EntityProps> entityProps) {
        String queryStart = "INSERT INTO " + tableName;

        String fieldNames = "(" + entityProps.get(0).getFieldName() + "," + entityProps.get(0).getFieldName().concat("_type");
        String values = "VALUES (" + getParamsQueryPart(entityProps);
        for (int i = 1; i < entityProps.size(); i++) {
             fieldNames = fieldNames.concat(", ").concat(entityProps.get(i).getFieldName())
             .concat(", ").concat(entityProps.get(i).getFieldName().concat("_type"));
       }
        return queryStart + fieldNames + ") " + values;
    }


    private String getParamsQueryPart(List<EntityProps> entityProps) {
        StringBuilder sb = new StringBuilder();
        int i = entityProps.size();
        while (i > 1) {
            sb.append("?, ?, ");
            i--;
        } sb.append("?, ?)");
        return sb.toString();
    }
}
