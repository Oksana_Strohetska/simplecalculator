package entities;

public class Cat extends Animal {

    private final String name;
    private final Integer age;

    public Cat (int id, String breed, String name, Integer age) {
        super(id, breed);
        this.age = age;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}
