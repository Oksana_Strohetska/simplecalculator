package entities;

public class Sousage extends Entity {

    //@entityPropsGenerator.IgnoreField
    private final Double weight;

    private final Integer length;

    public Sousage(int id, Double weight, Integer length) {
        super(id);
        this.length = length;
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public int getLength() {
        return length;
    }
}
