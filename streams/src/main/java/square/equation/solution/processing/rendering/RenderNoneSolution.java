package square.equation.solution.processing.rendering;

import square.equation.solution.ResultType;
import square.equation.solution.Solution;

public class RenderNoneSolution implements Render {

    @Override
    public boolean canRender (Solution solution) {
        return solution.getResultType() == ResultType.NONE;
    }

    @Override
    public void renderResults(Solution solution) {
        System.out.println("The solution is not natural number");
    }
}
