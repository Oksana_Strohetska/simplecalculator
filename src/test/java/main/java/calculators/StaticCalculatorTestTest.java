package main.java.calculators;

import main.java.calculators.impl.StaticCalculator;
import main.java.operations.imp.DivideOperation;
import main.java.operations.imp.MultiplyOperation;
import main.java.operations.imp.SubtractOperation;
import main.java.operations.imp.SumOperation;
import org.junit.jupiter.api.Test;

import java.util.function.BinaryOperator;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StaticCalculatorTestTest {
    private BinaryOperator<Double> sumOperation = new SumOperation();
    private BinaryOperator<Double> subtractOperation = new SubtractOperation();
    private BinaryOperator<Double> multiplyOperation = new MultiplyOperation();
    private BinaryOperator<Double> divideOperation = new DivideOperation();
    private Calculator staticCalculator = new StaticCalculator(sumOperation, subtractOperation, multiplyOperation, divideOperation);

    @Test
    public void testSumOperation() {
        assertEquals(staticCalculator.doOperation("+", 4.2, 3.0), 7.2, 0);
    }

    @Test
    public void testSubtractOperation() {
        assertEquals(staticCalculator.doOperation("-", 9.8, 6.2), 3.6, 0.01);
    }

    @Test
    public void testMultiplyOperation() {
        assertEquals(staticCalculator.doOperation("*", 3.0, 5.1), 15.3, 0.01);
    }

    @Test
    public void testDivideOperation() {
        assertEquals(staticCalculator.doOperation("/", 27.9, 3.0), 9.3, 0.01);
    }
}