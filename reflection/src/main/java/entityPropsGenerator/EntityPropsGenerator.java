package entityPropsGenerator;

import entities.Entity;
import exceptions.EntityPropsException;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class EntityPropsGenerator {
    private static List<EntityProps> getHierarchyFields(List<EntityProps> acc, Entity entity) {
        if (entity.getClass().getSuperclass() == Object.class) {
            return acc;
        }
        acc.addAll(Arrays.stream(entity.getClass().getSuperclass().getDeclaredFields())
                .map(field -> toEntityProps(field, entity, field.getType()))
                .collect(toList()));

        return getHierarchyFields(acc, entity);
    }

    private static List<EntityProps> getSuperClassFields(List<EntityProps> acc, Entity entity) {
        Class<?> currentObjectClass = entity.getClass();
        while (currentObjectClass.getSuperclass() != Object.class) {
            acc.addAll(Arrays.stream(currentObjectClass.getSuperclass().getDeclaredFields())
                    .filter(field -> !field.isAnnotationPresent(IgnoreField.class))
                    .map(field -> toEntityProps(field, entity, field.getType()))
                    .collect(toList())
            );

            currentObjectClass = currentObjectClass.getSuperclass();
        }
        return acc;
    }

    private static EntityProps toEntityProps(Field field, Entity entity, Class aclass) {
        try {
            field.setAccessible(true);
            return new EntityProps(field.getName(), field.get(entity).toString(), aclass);
        } catch (Exception e) {
            throw new EntityPropsException("Cannot get EntityProps", e);
        }
    }

    public static List<EntityProps> getEntityProps(Entity entity) {
        Class<? extends Entity> clazz = entity.getClass();
        List<EntityProps> fields = (Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> isNotIgnored(entity, field.getName()))
                .map(field -> toEntityProps(field, entity, field.getType())))
                .collect(toList());
        return getSuperClassFields(fields, entity);
//        getHierarchyFields(fields, entity);
    }

    public static void printAllFields(List<Entity> entities) {
        for (Entity ent : entities) {
            List<EntityProps> entityFields = getEntityProps(ent);
            for (EntityProps props : entityFields) {
                if (isNotIgnored(ent, props.getFieldName())) {
                    System.out.println(props);
                }
            }
        }
    }

    private static boolean isNotIgnored (Entity entity, String fieldName) {
        return Arrays.stream(entity.getClass().getDeclaredFields())
                .filter(field -> field.getName().equals(fieldName))
                .noneMatch(field -> field.isAnnotationPresent(IgnoreField.class));
    }
}
