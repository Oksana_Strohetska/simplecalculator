import db.connection.DBConfiguration;
import db.connection.DBConnectionProvider;
import entities.Car;
import entities.Cat;
import entities.Entity;
import entities.Sousage;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import queryPreparation.QueryPreparator;

import java.sql.SQLException;

import static entityPropsGenerator.EntityPropsGenerator.getEntityProps;


public class Main {

    public static void main(String[] args) throws SQLException{
        DBConnectionProvider dbConnectionProvider = new DBConnectionProvider(new DBConfiguration());
        dbConnectionProvider.getDBConnection();
        migrate();
        QueryRunner queryRunner = new QueryRunner(dbConnectionProvider);

        Entity car1 = new Car(1, 160, 2.0);
        QueryPreparator queryPreparator = new QueryPreparator(car1);
        queryRunner.runInsertQuery(getEntityProps(car1), queryPreparator.insertQueryPreparing("cars", getEntityProps(car1)));

        Entity car2 = new Car(2, 180, 2.4);
        queryRunner.runInsertQuery(getEntityProps(car2),
                new QueryPreparator(car2).insertQueryPreparing("cars", getEntityProps(car2)));

        Entity car3 = new Car(3, 120, 1.6);
        queryRunner.runInsertQuery(getEntityProps(car3),
                new QueryPreparator(car3).insertQueryPreparing("cars", getEntityProps(car3)));

        Entity car4 = new Car(4, 100, 1.2);
        queryRunner.runInsertQuery(getEntityProps(car4),
                new QueryPreparator(car4).insertQueryPreparing("cars", getEntityProps(car4)));

        Entity cat1 = new Cat(5,"Sphynx", "Foo", 2);
        queryRunner.runInsertQuery(getEntityProps(cat1),
                        new QueryPreparator(cat1).insertQueryPreparing("cats", getEntityProps(cat1)));

        Entity cat2 = new Cat(6,"Persian", "Bar", 4);
        queryRunner.runInsertQuery(getEntityProps(cat2),
                new QueryPreparator(cat2).insertQueryPreparing("cats", getEntityProps(cat2)));

        Entity cat3 = new Cat(7,"Maine Coon", "Baz", 3);
        queryRunner.runInsertQuery(getEntityProps(cat3),
                new QueryPreparator(cat3).insertQueryPreparing("cats", getEntityProps(cat3)));

        Entity cat4 = new Cat(8,"Devon Rex", "Boo", 8);
        queryRunner.runInsertQuery(getEntityProps(cat4),
                new QueryPreparator(cat4).insertQueryPreparing("cats", getEntityProps(cat4)));

        Entity sousage1 = new Sousage(9,50.5, 15);
        queryRunner.runInsertQuery(getEntityProps(sousage1),
                new QueryPreparator(sousage1).insertQueryPreparing("sousages", getEntityProps(sousage1)));

        Entity sousage2 = new Sousage(10,45.5, 11);
        queryRunner.runInsertQuery(getEntityProps(sousage2),
                new QueryPreparator(sousage2).insertQueryPreparing("sousages", getEntityProps(sousage2)));

        Entity sousage3 = new Sousage(11,60.5, 13);
        queryRunner.runInsertQuery(getEntityProps(sousage3),
                new QueryPreparator(sousage3).insertQueryPreparing("sousages", getEntityProps(sousage3)));

        Entity sousage4 = new Sousage(12,50.0, 12);
        queryRunner.runInsertQuery(getEntityProps(sousage4),
                new QueryPreparator(sousage4).insertQueryPreparing("sousages", getEntityProps(sousage4)));

//        List<Entity> entities = List.of(car1, car2, car3, car4,
//                cat1, cat2, cat3, cat4, sousage1, sousage2, sousage3, sousage4);
//        printAllFields(entities);

    }

    private static void migrate() {
        DBConfiguration dbConfig = new DBConfiguration();
        Flyway flyway = Flyway
                .configure()
                .dataSource("jdbc:postgresql://" +dbConfig.getDbHost() +":" + dbConfig.getDbPort() + "/" +
                        dbConfig.getDbName(), dbConfig.getDbUser(), dbConfig.getDbPass())
                .locations(new Location("db.migrations"))
                .load();
        flyway.baseline();
        flyway.migrate();
    }
}


