package user.sort;

import user.dto.User;

import java.util.Comparator;
import java.util.function.Function;

public class Asc implements Sort {
    private final Function<User, Comparable> keyMapper;
    private SortType sortType;

    public Asc(Function<User, Comparable> keyMapper) {
        this.keyMapper = keyMapper;
    }

    public Asc (Function<User, Comparable> keyMapper, SortType sortType) {
        this.keyMapper = keyMapper;
        this.sortType = SortType.ASC;
    }

    @Override
    public Comparator<User> getComparator() {
        return Comparator.comparing(keyMapper);
    }
}
