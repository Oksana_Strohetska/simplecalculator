package square.equation.calculation;

import square.equation.solution.impl.SingleSolution;
import square.equation.solution.Solution;

public class ZeroDiscriminantEquation extends SquareEquation {
    @Override
    public boolean isCalculated(double discriminant) {
        return discriminant == 0.;
    }

    @Override
    public Solution calculate(int a, int b, double discriminant) {
        return new SingleSolution((double) (-b / (2 * a)));
    }
}
