package user.repository;

import user.dto.User;

import java.util.List;

public class UserRepositoryDB implements UserRepository {
    @Override
    public User add(User user) {
        return null;
    }

    @Override
    public long add(List<User> users) {
        return 0;
    }

    @Override
    public User get(int id) {
        return null;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public User update(User user) {
        return null;
    }

    @Override
    public boolean delete(User user) {
        return false;
    }

    @Override
    public void deleteAll() {

    }
}
